# Copyright 1999-2018 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=5
inherit flag-o-matic multilib

DESCRIPTION="LV2 Noise Gate plugin"
HOMEPAGE="http://abgate.sourceforge.net/"
SRC_URI="https://github.com/antanasbruzas/${PN}/archive/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="GPL-3"
KEYWORDS="~amd64 ~x86"
SLOT="0"
IUSE=""

RDEPEND="dev-cpp/gtkmm:2.4
	dev-qt/qtcore:5
	dev-qt/qtgui:5
	media-libs/lv2"
DEPEND="${RDEPEND}
	virtual/pkgconfig"

RESTRICT="mirror"

DOCS=(README.md ChangeLog)

src_install() {
	default INSTALL_DIR="${ED}"/usr/$(get_libdir)/lv2
}
