# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=6

DESCRIPTION="A collection of bridges which allow using DSSI and LADSPA plug-ins in LV2 hosts"
HOMEPAGE="http://naspro.sourceforge.net/"
SRC_URI="mirror://sourceforge/naspro/naspro/${PV}/${P}.tar.bz2"

LICENSE="LGPL-2.1"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="static-libs"

RDEPEND="media-libs/alsa-lib
	>=media-libs/naspro-core-0.5.1
	>=media-libs/naspro-bridge-it-0.5.1
	media-libs/lv2
	media-libs/ladspa-sdk
	media-libs/dssi"
DEPEND="${RDEPEND}
	virtual/pkgconfig"

RESTRICT="mirror"

DOCS=( AUTHORS ChangeLog NEWS README THANKS )
