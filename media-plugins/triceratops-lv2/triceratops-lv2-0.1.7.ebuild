# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=6

PYTHON_COMPAT=( python2_7 )
PYTHON_REQ_USE="threads(+)"
inherit python-r1 waf-utils

REQUIRED_USE="${PYTHON_REQUIRED_USE}"

MY_P=triceratops-lv2-v${PV}
DESCRIPTION="A polyphonic subtractive synthesizer plugin for use with the LV2 architecture"
HOMEPAGE="http://sourceforge.net/projects/triceratops/"
SRC_URI="mirror://sourceforge/triceratops/${MY_P}.tar.gz"

S=${WORKDIR}/${MY_P}

LICENSE="GPL-3+"
SLOT="0"
KEYWORDS="~amd64"
IUSE=""

DOCS=( README )

RDEPEND="media-libs/lv2"
DEPEND="${RDEPEND}
	>=dev-cpp/gtkmm-2.24"

src_configure() {
	python_setup
	waf-utils_src_configure
}
