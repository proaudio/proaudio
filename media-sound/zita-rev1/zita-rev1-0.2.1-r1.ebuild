# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=6
inherit eutils toolchain-funcs

DESCRIPTION="Zita-rev1 is a reworked version of the reverb originally developed for Aeolus"
HOMEPAGE="http://kokkinizita.linuxaudio.org/linuxaudio/"
#SRC_URI="http://kokkinizita.linuxaudio.org/linuxaudio/downloads/${P}.tar.bz2"
SRC_URI="http://download.tuxfamily.org/proaudio/distfiles/${P}.tar.bz2"

RESTRICT="mirror"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="doc"

DEPEND=">=media-libs/libclthreads-2.4.0
	>=media-libs/libclxclient-3.6.1
	virtual/jack"
RDEPEND="${RDEPEND}"

DOCS=( ../AUTHORS ../README )
HTML_DOCS=( ../doc/ )

PATCHES=("${FILESDIR}/${P}-makefile.patch")

S="${WORKDIR}/${P}/source"

src_compile() {
	default CXX="$(tc-getCXX)" PREFIX=/usr
}

src_install() {
	emake DESTDIR="${D}" PREFIX=/usr install

	einstalldocs
}
