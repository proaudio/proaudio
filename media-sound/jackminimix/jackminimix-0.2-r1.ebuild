# Copyright 1999-2018 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI="6"

inherit autotools

DESCRIPTION="A simple mixer for JACK with an OSC based control interface"
HOMEPAGE="http://www.aelius.com/njh/jackminimix"
SRC_URI="http://www.aelius.com/njh/${PN}/${P}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

RDEPEND=">=media-sound/jack-audio-connection-kit-0.100
		>=media-libs/liblo-0.23"

DEPEND="${RDEPEND}"

PATCHES="${FILESDIR}/${P}-update_email_and_website.patch"
DOCS=(README AUTHORS TODO ChangeLog)

src_prepare() {
	default
	eautoreconf
}
