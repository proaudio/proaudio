# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=5

DESCRIPTION="Portable runtime library of the NASPRO Architecture for Sound PROcessing"
HOMEPAGE="http://naspro.sourceforge.net/"
SRC_URI="mirror://sourceforge/naspro/naspro/${PV}/${P}.tar.bz2"

LICENSE="LGPL-2.1"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="static-libs"

RESTRICT="mirror"

DOCS=( AUTHORS ChangeLog NEWS README THANKS )

src_prepare() {
	# fix build error coming from deprecation warning
	sed -i 's:_BSD_SOURCE:_DEFAULT_SOURCE:g' src/posix/fs.c

	default
}
